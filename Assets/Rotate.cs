﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
    float speed;
	// Use this for initialization
	void Start () {
        float rand = Random.value;
        int sign = 1;
        if (rand < 0) sign = -1;

        speed = rand * 2 + 1;
        
        speed *= 180 * sign;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, speed * Time.deltaTime);
	}
}
