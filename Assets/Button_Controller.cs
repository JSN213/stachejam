﻿using UnityEngine;
using System.Collections;

public class Button_Controller : MonoBehaviour {
    SpriteRenderer sprite;
	// Use this for initialization
	void Start () {

        sprite = GetComponent<SpriteRenderer>();
        Utilities.Set_Transparency(sprite, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        print("hello");
        if (other.tag == "Player")
        {
            Utilities.Set_Transparency(sprite, 1);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Utilities.Set_Transparency(sprite, 0);
        }
    }
}
