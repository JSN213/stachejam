using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    //Pitch variables
    private float min_pitch = 0.90f;
    private float max_pitch = 1.15f;
    //Main variables
    private Sound_Dictionnary sound_dicto;
    private Object_Dictionary obj_dicto;


    #region Audioclip variables
    private AudioClip _shoot;
    private AudioClip _jump;
    private AudioClip _drop;
    private AudioClip _eating;
    private AudioClip _swing_hammer;
    private AudioClip _hit_hammer;
    private AudioClip _rock_textures;
    private AudioClip _get_socks;
    private AudioClip _acquire_obj;
    private AudioClip _get_music;
    private AudioClip _compo_principal;
    private AudioClip[] _fstepsR;

    private AudioSource[] Chords_sources;
    GameObject Chord_Obj;

    public enum collide_object { ground, raft, washer, rock };
    public enum collide_hammer { ground = 1, washer, water, tree }


    #endregion
    #region AudioSource variables
    private AudioSource player_source;

    private AudioSource shoot_source;

    private AudioSource music_source;

    private AudioSource ambience_source;
    private AudioSource Hurting_source;

    #endregion
    #region Mixer variables
    public AudioMixer main_mixer;
    private AudioMixerSnapshot[] main_snapshots;
    public AudioMixer ennemis_mixer;

    public AudioMixerSnapshot[] shoot_snapshots;

    AudioMixer ambience_mixer;
    public AudioMixer player_mixer;
    public string footsteps_track;

    public AudioMixer mecaniques_mixer;

    public AudioMixer geyser_mixer;
    public AudioMixerSnapshot[] geyser_pressure;
    public string geyser_track;

    public AudioMixer manette_mixer;
    public AudioMixerSnapshot[] beep_fader;

    public AudioMixerSnapshot[] screechTransitions;


    public AudioMixer music_mixer;

    float[] j = new float[] { 0f, 1f };
    private string[] player_mixers = { "Jump", "Wobble", "Screech", "Damage/Death" };
    private string[] music_mixers = { "Music", "Notes", "Chords" };
    private string[] ennemis_mixers = { "Shoot", "Destroy", "DestroyShip" };
    private string[] ambience_mixers = { "Vitre" };
    float number;
    #endregion

    
    void Awake()
    {

        obj_dicto = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        sound_dicto = GameObject.Find("SoundManager").GetComponent<Sound_Dictionnary>();
        Hurting_source = GameObject.Find("Hurting_Audiosource").GetComponent<AudioSource>();
        ennemis_mixer = (AudioMixer)Resources.Load("Mixers/Ennemis", typeof(AudioMixer));
        music_mixer = (AudioMixer)Resources.Load("Mixers/Music", typeof(AudioMixer));
        ambience_mixer = (AudioMixer)Resources.Load("Mixers/Ambience", typeof(AudioMixer));
        player_mixer = (AudioMixer)Resources.Load("Mixers/Player", typeof(AudioMixer));
        mecaniques_mixer = (AudioMixer)Resources.Load("Mixers/Mecaniques", typeof(AudioMixer));
        shoot_snapshots = new AudioMixerSnapshot[2];
        shoot_snapshots[0] = ennemis_mixer.FindSnapshot("Shoot");
        shoot_snapshots[1] = ennemis_mixer.FindSnapshot("Unshoot");
        main_mixer = (AudioMixer)Resources.Load("Mixers/Main", typeof(AudioMixer));
        main_snapshots = new AudioMixerSnapshot[3];
        main_snapshots[0] = main_mixer.FindSnapshot("Normal");
        main_snapshots[1] = main_mixer.FindSnapshot("Wobble_FX");

        screechTransitions = new AudioMixerSnapshot[3];
        screechTransitions[0] = player_mixer.FindSnapshot("ScreechLow");
        screechTransitions[1] = player_mixer.FindSnapshot("ScreechHigh");





    }
    void Start()
    {
        //Get sources
        player_source = obj_dicto.Get_Player().GetComponent<AudioSource>();

        music_source = GameObject.Find("Music_Audiosource").GetComponent<AudioSource>();
        ambience_source = GameObject.Find("Ambience_Audiosource").GetComponent<AudioSource>();
        Chord_Obj = GameObject.Find("Chords_Audiosource");
        int k = 0;
        Chords_sources = new AudioSource[3];
        foreach (Transform child in Chord_Obj.transform)
        {
            Chords_sources[k] = child.GetComponent<AudioSource>();
            k++;
        }

        if (ambience_source.ToString() == "null")
        {


            player_source = GameObject.Find("Ambience_Audiosource").AddComponent<AudioSource>();
            //player_source.outputAudioMixerGroup = player_mixer;
        }
        _shoot = sound_dicto.ennemis_sounds[0];
        if (player_source.ToString() == "null")
        {


            player_source = obj_dicto.Get_Player().AddComponent<AudioSource>();
            //player_source.outputAudioMixerGroup = player_mixer;
        }

            

    }
    

    
    // Player sounds
    public void Start_OneHeart()
    {
        playSmpl(sound_dicto.player_interactions[3], Hurting_source, player_mixer, "Close2Death", 1f, 1f, false);
        Hurting_source.loop = true;
    }
    public void Stop_OneHeart()
    {
        Hurting_source.Stop();
    }
    public IEnumerator play_Screech()
    {
        
            screechTransitions[1].TransitionTo(0.3f);
            playSmpl(sound_dicto.player_interactions[2], player_source, player_mixer, player_mixers[2], 0.85f, 0.95f, true);
            yield return new WaitForSeconds(0.3f);
            screechTransitions[0].TransitionTo(0f);
        
        
    }
    public void play_PlayerHurt()
    {
        playRndSmpl(sound_dicto.player_hurt, player_source, player_mixer, player_mixers[3], 0.8f, 0.9f, false);
    }
    public void play_GetNote(int note_index)
    {
        playSmpl(sound_dicto.notes[note_index], ambience_source, music_mixer, music_mixers[1], 1f, 1f, false);
    }
    public void play_NoteBurst(int[] index)
    {
        playSmpl(sound_dicto.notes[index[0]], Chords_sources[0], music_mixer, music_mixers[2], 1f, 1f, false);
        playSmpl(sound_dicto.notes[index[1]], Chords_sources[1], music_mixer, music_mixers[2], 1f, 1f, false);
        playSmpl(sound_dicto.notes[index[2]], Chords_sources[2], music_mixer, music_mixers[2], 1f, 1f, false);
    }
    public void Destroy_Turret()
    {
        playSmpl(sound_dicto.ennemis_sounds[2], ambience_source, ennemis_mixer, ennemis_mixers[1], 0.7f, 1.3f, false);
    }
    public void Destroy_Spaceship()
    {
        playSmpl(sound_dicto.ennemis_sounds[3], ambience_source, ennemis_mixer, ennemis_mixers[2], 0.7f, 1.3f, false);
    }
    public IEnumerator Play_Wobble()
    {
        if (!player_source.isPlaying)
        {
            playSmpl(sound_dicto.player_interactions[1], player_source, player_mixer, player_mixers[0], 0.85f, 0.95f, false);
            main_snapshots[1].TransitionTo(0f);
            yield return new WaitForSeconds(0.1f);
            playSmpl(sound_dicto.mecanics[0], Chords_sources[0], mecaniques_mixer, player_mixers[1], 1f, 1f, false);
            yield return new WaitForSeconds(1f);
            main_snapshots[0].TransitionTo(0.5f);
        }
        
    }

    //Ennemis sounds
    public void play_shoot(AudioSource source)
    {
        playSmpl(_shoot, source, ennemis_mixer, ennemis_mixers[0], min_pitch, max_pitch, false);

    }
    public void play_Portal()
    {
        playSmpl(sound_dicto.player_interactions[3], ambience_source, ambience_mixer, "Portal", min_pitch, max_pitch, false);
    }
    public void Play_VitreBriser( )
   {
       playSmpl(sound_dicto.ennemis_sounds[1], ambience_source, ambience_mixer, ambience_mixers[0], min_pitch, max_pitch, false);
   }
   
    #region Getters
    public AudioMixerGroup Get_ShootMixer()
    {
        return ennemis_mixer.FindMatchingGroups(ennemis_mixers[0])[0];
    }

    #endregion

    #region Utility methods
    private void playRndSmpl(AudioClip[] soundArray, AudioSource source, AudioMixer mixer, string mixername, float pitchmin, float pitchmax, bool is_pan)
    {
        int max = soundArray.Length;

        source.clip = soundArray[Random.Range(0, max)];
        source.outputAudioMixerGroup = mixer.FindMatchingGroups(mixername)[0];
        if (is_pan)
        {
            source.panStereo = Random.Range(-0.3f, 0.3f);
        }
        source.pitch = Random.Range(pitchmin, pitchmax);
        source.loop = false;
        source.Play();
        
    }
    private IEnumerator playProcedure(AudioSource source, float seconds = 0)
    {
        source.Play();
        yield return new WaitForSeconds(seconds);
        //source.Stop();
        
    }
    private void playSmpl(AudioClip sample, AudioSource source,AudioMixer mixer, string mixername, float pitchmin, float pitchmax, bool is_panned)
    {
        
        source.clip = sample;
        source.outputAudioMixerGroup = mixer.FindMatchingGroups(mixername)[0];
        if (is_panned)
        {
            source.panStereo = Random.Range(-0.3f, 0.3f);
        }
        source.pitch = Random.Range(pitchmin, pitchmax);
        source.loop = false;
        source.Play();
    }
    public int GetRandomValue(int min, int max)
    {
        return Random.Range(min, max);
    }

    #endregion
        














































    #region old code
    /*
    public double waittime = 0;
    public double limit = 100;
    public bool Soundisplaying;
    public float ShotVolMin = 0.8f;
    public float ShotVolMax= 1.2f;
    public float FrequencyMin;
    public float FrequencyMax;
    public float timeBetweenBullets = 0.15f;

    public DarkArtsStudios.SoundGenerator.Composition GunshotPrefab;
    private DarkArtsStudios.SoundGenerator.Module.Output _toneOutput;

    private AudioClip Sound0;

    float timer;

    public AudioSource GunshotAudiosource;
    private AudioClip _gunshotAudioClip;
    private AudioClip _sound_clip;
    public Dictionary<int,AudioClip> DictoAudioClips = new Dictionary<int, AudioClip>();
    private int counter = 0;

    #region Generate sound method

    public void GenerateSound()
    {
        _toneOutput.attribute("Frequency").value = Random.Range(FrequencyMin, FrequencyMax);
        _toneOutput.Generate();
        _sound_clip = _toneOutput.audioClip;
        GunshotAudiosource.loop = false;
    }




    #endregion


    #region Unity Hooks

    void Start()
    {

        GunshotAudiosource = GetComponent<AudioSource>();
        // Retrieve toneOutput Module
        _toneOutput =GunshotPrefab.modules.Find(x => x.GetType() == typeof (DarkArtsStudios.SoundGenerator.Module.Output)) as DarkArtsStudios.SoundGenerator.Module.Output;

        GenerateSound();
        Sound0 = _gunshotAudioClip;
        DictoAudioClips.Add(counter,Sound0);



        GunshotAudiosource.clip = DictoAudioClips[Random.Range(0, DictoAudioClips.Count-1)];

    }




    void Update()
    {

        timer += Time.deltaTime;
        Soundisplaying = GunshotAudiosource.isPlaying;



        if (waittime > limit)
        {
            DictoAudioClips.Remove(counter);
            GenerateSound();
            Sound0 = _gunshotAudioClip;
            DictoAudioClips.Add(counter, Sound0);

        }

        else if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            GunshotAudiosource.pitch = Random.Range(0.9f, 1.1f);
            GunshotAudiosource.panStereo = Random.Range(-0.3f, 0.3f);
            if (Soundisplaying)
            {
                GunshotAudiosource.loop = true;
                timer = 0;
            }
            else if(!Soundisplaying)
            {
                GunshotAudiosource.PlayOneShot(GunshotAudiosource.clip, Random.Range(ShotVolMin, ShotVolMax));
                GunshotAudiosource.clip = DictoAudioClips[Random.Range(0, DictoAudioClips.Count - 1)];
            }



        }
        waittime += 0.5;
    }







    #endregion
    
   

    */
     #endregion
}