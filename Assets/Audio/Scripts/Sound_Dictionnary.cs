﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class Sound_Dictionnary : MonoBehaviour {
    public Dictionary<string, AudioClip> sound_dic;
    public AudioClip[] notes;
    public AudioClip[] player_interactions;
    public AudioClip[] drop_textures;
    public AudioClip[] mecanics;
    public AudioClip[] hammer_textures;
    public AudioClip[] socks_sounds;
    public AudioClip[] ennemis_sounds;
    public AudioClip[] music;
    public AudioClip[] player_hurt;
    private string ressources_getter;

    public Dictionary<string, AudioClip> Mec_dict = new Dictionary<string, AudioClip>();
    void Awake()
    {
        notes = new AudioClip[12];
        for (int k = 0; k < 12; k++)
        {
            ressources_getter = "Samples/" + k ;
            notes[k] = (AudioClip)Resources.Load(ressources_getter, typeof(AudioClip));
        }
        player_hurt = new AudioClip[3];
        player_hurt[0] = (AudioClip)Resources.Load("Samples/hurt1", typeof(AudioClip));
        player_hurt[1] = (AudioClip)Resources.Load("Samples/hurt2", typeof(AudioClip));
        player_hurt[2] = (AudioClip)Resources.Load("Samples/hurt3", typeof(AudioClip));

        ennemis_sounds = new AudioClip[4];
        ennemis_sounds[0] = (AudioClip)Resources.Load("Samples/laser", typeof(AudioClip));
        ennemis_sounds[1] = (AudioClip)Resources.Load("Samples/vitre", typeof(AudioClip));
        ennemis_sounds[2] = (AudioClip)Resources.Load("Samples/destroy_ennemis", typeof(AudioClip));
        ennemis_sounds[3] = (AudioClip)Resources.Load("Samples/destroy_spaceship", typeof(AudioClip));
        player_interactions = new AudioClip[5];
        player_interactions[1] = (AudioClip)Resources.Load("Samples/yeah_wobble", typeof(AudioClip));
        player_interactions[2] = (AudioClip)Resources.Load("Samples/wobble_cry", typeof(AudioClip));
        player_interactions[3] = (AudioClip)Resources.Load("Samples/portal", typeof(AudioClip));
        player_interactions[4] = (AudioClip)Resources.Load("Samples/One_heart", typeof(AudioClip));

        mecanics = new AudioClip[2];
        mecanics[0] = (AudioClip)Resources.Load("Samples/Wobble", typeof(AudioClip));
    }
    private void makeDictWArray(AudioClip[] soundErray,Dictionary<string,AudioClip> p_dicto )
    {
        int arrayLength = soundErray.Length;
        for(int j = 0; j < arrayLength; j++)
        {
             string str = soundErray[j].ToString();
             AudioClip clip = soundErray[j];
             str.Trim();
             print(str);
             p_dicto.Add(str, clip);
        }
        

    }

}

