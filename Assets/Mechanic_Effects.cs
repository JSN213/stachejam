﻿using UnityEngine;
using System.Collections;

public class Mechanic_Effects : MonoBehaviour {

    GameObject circle;
    GameObject screech;

    Sprite[] sprites = new Sprite[3];

	// Use this for initialization
	void Start () {
        circle = (GameObject)Resources.Load("Mecaniques/Circle", typeof(GameObject));
        screech = (GameObject)Resources.Load("Mecaniques/Screech", typeof(GameObject));


        sprites[0] = (Sprite)Resources.Load("Textures/FruitsR", typeof(Sprite));
        sprites[1] = (Sprite)Resources.Load("Textures/LightR", typeof(Sprite));
        sprites[2] = (Sprite)Resources.Load("Textures/PurpleRR", typeof(Sprite));
    }

    // Update is called once per frame
    void Update () {

    }

    public void IWobble(Vector3 position, float delay = 0)
    {
        StartCoroutine(IWobble_Effect(position, delay));
    }

    public void Wobble(Transform t, float delay = 0)
    {
        StartCoroutine(Wobble_Effect(t, delay));
    }

    float circle_buffer = 0.1f;
    public IEnumerator IWobble_Effect(Vector3 position, float delay)
    {
        if (delay != 0)
        {
            yield return new WaitForSeconds(delay);
        }
        int n = Random.Range(0, sprites.Length);
        StartCoroutine(IPop_Circle(position, sprites[n], 3));
        yield return new WaitForSeconds(circle_buffer);
        StartCoroutine(IPop_Circle(position, sprites[(n + 1) % sprites.Length], 2));
        yield return new WaitForSeconds(circle_buffer);
        StartCoroutine(IPop_Circle(position, sprites[(n + 2) % sprites.Length], 1));
    }

    public IEnumerator Wobble_Effect(Transform t, float delay)
    {
        if (delay != 0)
        {
            yield return new WaitForSeconds(delay);
        }
        int n = Random.Range(0, sprites.Length);
        StartCoroutine(Pop_Circle(t, sprites[n], 3));
        yield return new WaitForSeconds(circle_buffer);
        StartCoroutine(Pop_Circle(t, sprites[(n+1)%sprites.Length], 2));
        yield return new WaitForSeconds(circle_buffer);
        StartCoroutine(Pop_Circle(t, sprites[(n + 2) % sprites.Length], 1));
    }

    float circle_time = 0.5f;
    float fade_time = 0.2f;
    Vector3 min_scale = Vector3.zero;
    Vector3 max_scale = Vector3.one * 6;

    public IEnumerator IPop_Circle(Vector3 position, Sprite color, float order)
    {
        Transform clone;
        SpriteRenderer sprite;
        //Instantiate
        
        if (transform.parent != null)
        {
            if (transform.parent.name == "Keyboard")
            {
                position = Camera.main.ScreenToWorldPoint(transform.position);
            }
        }

        clone = ((GameObject)Instantiate(circle, position + Vector3.forward * order, Quaternion.identity)).transform;
        //clone.parent = t;
        sprite = clone.gameObject.GetComponent<SpriteRenderer>();
        sprite.sprite = color;

        StartCoroutine(Zoom(clone));
        yield return new WaitForSeconds(circle_time - fade_time);
        yield return StartCoroutine(Utilities.Fade(sprite, .2f, 1, 0));
        Destroy(clone.gameObject);
        // FAde out
    }
    public IEnumerator Pop_Circle(Transform t, Sprite color, float order)
    {
        Transform clone;
        SpriteRenderer sprite;
        //Instantiate

        Vector3 position = t.position;
        if (transform.parent != null)
        {
            if (transform.parent.name == "Keyboard")
            {
                position = Camera.main.ScreenToWorldPoint(transform.position);
            }
        }

        clone = ((GameObject)Instantiate(circle, position + t.forward * order, Quaternion.identity)).transform;
        clone.parent = t;
        sprite = clone.gameObject.GetComponent<SpriteRenderer>();
        sprite.sprite = color;

        StartCoroutine(Zoom(clone));
        yield return new WaitForSeconds(circle_time - fade_time);
        yield return StartCoroutine(Utilities.Fade(sprite, .2f, 1, 0));
        Destroy(clone.gameObject);
        // FAde out
    }
    
    IEnumerator Zoom(Transform clone)
    {
        clone.localScale = min_scale;
        // Zoom out
        float timer = 0f;
        float t;
        while (timer < circle_time)
        {
            yield return null;
            if (clone == null)
            {
                break;
            }

            t = 1 - timer / circle_time;
            t = 1 - (t * t * t);

            clone.localScale = Vector3.Lerp(min_scale, max_scale, t);

            timer += Time.deltaTime;
        }

        if (clone != null)
            clone.localScale = max_scale;
    }
}
