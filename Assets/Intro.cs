﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Intro : MonoBehaviour {
    Image[] images;

	// Use this for initialization
	void Start () {
        images = GetComponentsInChildren<Image>();

        StartCoroutine(Introduction());
	}
	
    IEnumerator Introduction()
    {
        Time.timeScale = 0;
        for (int i = 0; i < images.Length; i++)
        {
            Utilities.Set_Transparency(images[i], 1);
//StartCoroutine(Utilities.Fade(images[i], time_fade, 1, 0, true));
        }
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(2));// WaitForSeconds(2);
        float time_fade = 1f;

        for (int i = 0; i < images.Length; i++)
        {
            //Utilities.Set_Transparency(images[i], 1);
            StartCoroutine(Utilities.Fade(images[i], time_fade, 1, 0, true));
        }
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(time_fade));// WaitForSeconds(2);
        Time.timeScale = 1;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
