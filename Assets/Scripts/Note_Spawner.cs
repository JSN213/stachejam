﻿using UnityEngine;
using System.Collections;

public class Note_Spawner : MonoBehaviour {
    float speed = -5;
    float d_speed;
    float buffer = 0.5f;
    float rate = .3f;
    Rigidbody2D rb;
    GameObject note;

    float min_y = -5f;
    float max_y = 15f;
    
	void Awake () {
        rb = GetComponent<Rigidbody2D>();
        note = (GameObject)Resources.Load("Prefabs/Note", typeof(GameObject));
    }
	
	// Update is called once per frame
	void Update () {
        d_speed = (rb.velocity.x - speed)/-speed;

        if (d_speed > 0.5)
        {

            float prob = (1 - Mathf.Pow(rate, d_speed)) * Time.deltaTime;
            if (Random.value < prob)
            {
                float y = Mathf.Lerp(transform.position.y + min_y, transform.position.y + max_y, Random.value);
                Vector3 position = transform.position + Vector3.right * 25;
                position.y = y;
                Instantiate(note, position, Quaternion.identity);
            }
        }
	}
}
