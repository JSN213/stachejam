﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Object_Dictionary : MonoBehaviour {
    [HideInInspector]
    public Transform[] checkpoints;

    GameObject game_controller;
    GameObject player;

    UI_Manager ui_manager;
    Game_Controller gc;
    Health_Manager hm;

    List<GameObject> visibles;

	// Use this for initialization
	void Awake () {
        visibles = new List<GameObject>();
        game_controller = GameObject.Find("Game Controller");
        player = GameObject.Find("Player");

        ui_manager = game_controller.GetComponent<UI_Manager>();
        gc = game_controller.GetComponent<Game_Controller>();
        hm = player.GetComponent<Health_Manager>();

        GameObject temp = GameObject.Find("Checkpoints");

        checkpoints = new Transform[temp.transform.childCount];
        int i = 0;
        foreach (Transform child in temp.transform)
        {
            checkpoints[i] = child;
            i++;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Vector3 To_Checkpoint(int i)
    {
        return checkpoints[i].transform.position;
    }

    public UI_Manager Get_UI_Manager()
    {
        return ui_manager;
    }

    public Game_Controller Get_Game_Controller()
    {
        return gc;
    }

    public Health_Manager Get_Health_Manager()
    {
        return hm;
    }
    public GameObject Get_Player()
    {
        return player;
    }

    public void Set_Visible(GameObject visible)
    {
        visibles.Add(visible);
    }

    public void Set_Invisible(GameObject invisible)
    {
        visibles.Remove(invisible);
    }


    public void Destroy_Visible()
    {
        foreach (GameObject visible in visibles)
        {
            GameObject.Find("Player").GetComponent<Mechanic_Effects>().IWobble(visible.transform.position);
            if (visible.transform.tag == "Turret")
            {
                Destroy(visible.transform.parent.gameObject);
            }
            else
            {
                Destroy(visible);
            }
        }
        visibles = new List<GameObject>();
    }

    public void Highlight_Zoom(GameObject target)
    {
        StartCoroutine(Highlight_Zoom_C(target));
    }
    
    public void Highlight_Zoom(Image target)
    {
        StartCoroutine(Highlight_Zoom_C(target));
    }

    public IEnumerator Highlight_Zoom_C(GameObject target)
    {
        float zoom_time = 0.5f;
        float fade_time = 0.2f;
        float alpha = 1;// .5f;
        GameObject highlight = new GameObject();
        //highlight.transform.parent = target.transform;
        highlight.transform.position = target.transform.position;
        highlight.transform.rotation = target.transform.rotation;
        highlight.transform.localScale = target.transform.localScale;

        SpriteRenderer sprite = target.GetComponent<SpriteRenderer>();
        SpriteRenderer new_sprite = highlight.AddComponent<SpriteRenderer>();

        new_sprite.sprite = sprite.sprite;
        new_sprite.color = sprite.color;
        Utilities.Set_Transparency(new_sprite, alpha);
        StartCoroutine(Utilities.Resize(highlight.transform, zoom_time, 1, 2));
        yield return new WaitForSeconds(zoom_time - fade_time);
        yield return StartCoroutine(Utilities.Fade(new_sprite, fade_time, alpha, 0));
        Destroy(highlight);
    }

    public IEnumerator Highlight_Zoom_C(Image target)
    {
        float zoom_time = 0.5f;
        float fade_time = 0.2f;
        float alpha = 1;// .5f;

        GameObject highlight = new GameObject();
        highlight.transform.parent = target.transform.parent;
        //highlight.transform.parent = target.transform;
        highlight.transform.position = target.transform.position + Vector3.back * 50;
        highlight.transform.rotation = target.transform.rotation;
        //highlight.transform.localScale = target.transform.localScale;

        Image image = target;
        Image new_image = highlight.AddComponent<Image>();
        new_image.preserveAspect = true;
        new_image.sprite = image.sprite;
        new_image.color = image.color;
        //Utilities.Set_Transparency(new_image, alpha);
        //StartCoroutine(effects.Shine());

        StartCoroutine(Utilities.Move(highlight.transform, zoom_time, 0, 50));
        StartCoroutine(Utilities.Resize(highlight.transform, zoom_time, 1, 2));
        yield return new WaitForSeconds(zoom_time - fade_time);
        yield return StartCoroutine(Utilities.Fade(new_image, fade_time, alpha, 0));
        Destroy(highlight);
    }
}
