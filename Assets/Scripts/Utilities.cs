﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;


public class Utilities : MonoBehaviour
{
    public static Color transparent = new Color(1.0f, 1.0f, 1.0f, 0.0f);

    public static int[] DeepCopy(int[] _array)
    {
        int[] array = new int[_array.Length];

        for (int i = 0; i < _array.Length; i++)
        {
            array[i] = _array[i];
        }

        return array;
    }

    public static string[] groundable = {"Tile"};
    public static bool Groundable(string tag)
    {
        for (int i = 0; i < groundable.Length; i++)
        {
            if (tag == groundable[i]) return true;
        }
        return false;
    }

    public static void Set_Transparency(SpriteRenderer sprite, float alpha)
    {
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, alpha);
    }

    public static void Set_Transparency(Text text, float alpha)
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, alpha);
    }

    public static void Set_Transparency(Material material, float alpha)
    {
        material.color = new Color(material.color.r, material.color.g, material.color.b, alpha);
    }

    public static void Set_Transparency(Image image, float alpha)
    {

        image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    }

    public static Vector3 Clamp(Vector3 value, Vector3 vector1, Vector3 vector2)
    {
        float max = Mathf.Max(vector1.x, vector2.x);
        float min = Mathf.Max(vector1.x, vector2.x);
        float x = Mathf.Clamp(value.x, min, max);

        max = Mathf.Max(vector1.x, vector2.x);
        min = Mathf.Max(vector1.x, vector2.x);
        float y = Mathf.Clamp(value.x, min, max);

        max = Mathf.Max(vector1.x, vector2.x);
        min = Mathf.Max(vector1.x, vector2.x);
        float z = Mathf.Clamp(value.x, min, max);

        return new Vector3(x, y, z);
    }

    public static Vector3 string2vector(string mystr)
    {
        string newstr = mystr.Trim(new char[] { '(', ')' });
        string[] chars;

        chars = newstr.Split(',');

        Vector3 vect = new Vector3(Convert.ToSingle(chars[0]), Convert.ToSingle(chars[1]), Convert.ToSingle(chars[2]));

        return vect;
    }


    public static int Type2Int(string type)
    {
        switch (type)
        {
            case ("Green"):
                return 0;

            case ("Yellow"):
                return 1;

            case ("Red"):
                return 2;

            case ("Gold"):
                return 3;

            case ("Dark"):
                return 4;

            case ("Filthy"):
                return 5;

            default:
                throw new System.Exception("Sock type '" + type + "' unknown.");

        }
    }

    public static IEnumerator Resize(Transform object_transform, float time, float initial, float final, bool is_unscaled = true)
    {
        Vector3 initial_scale = Vector3.one * initial;
        Vector3 final_scale = Vector3.one * final;

        float timer = 0.0f;
        Vector3 new_scale;

        object_transform.localScale = initial_scale;

        while (timer < time)
        {
            yield return null;
            new_scale = Vector3.Lerp(initial_scale, final_scale, timer / time);
            object_transform.localScale = new_scale;
            timer += Time.deltaTime;

            if (is_unscaled)
            {
                timer += Time.unscaledDeltaTime;
            }
        }
        
        object_transform.localScale = final_scale;
    }

    public static IEnumerator Fade(Image image, float time, float initial, float final, bool is_unscaled = true)
    {
        float timer = 0.0f;
        float new_alpha;

        Set_Transparency(image, initial);

        while (timer < time)
        {
            yield return null;
            new_alpha = Mathf.Lerp(initial, final, timer / time);
            Set_Transparency(image, new_alpha);
            timer += Time.deltaTime;

            if (is_unscaled)
            {
                timer += Time.unscaledDeltaTime;
            }
        }

        Set_Transparency(image, final);
    }


    public static IEnumerator Fade(SpriteRenderer sprite, float time, float initial, float final, bool is_unscaled = true)
    {
        float timer = 0.0f;
        float new_alpha;

        Set_Transparency(sprite, initial);

        while (timer < time)
        {
            yield return null;
            new_alpha = Mathf.Lerp(initial, final, timer / time);
            Set_Transparency(sprite, new_alpha);
            timer += Time.deltaTime;

            if (is_unscaled)
            {
                timer += Time.unscaledDeltaTime;
            }
        }

        Set_Transparency(sprite, final);
    }

    public static IEnumerator Fade(Text text, float time, float initial, float final, bool is_unscaled = true)
    {
        float timer = 0.0f;
        float new_alpha;

        Set_Transparency(text, initial);

        while (timer < time)
        {
            yield return null;
            new_alpha = Mathf.Lerp(initial, final, timer / time);
            Set_Transparency(text, new_alpha);
            timer += Time.deltaTime;

            if (is_unscaled)
            {
                timer += Time.unscaledDeltaTime;
            }
        }

        Set_Transparency(text, final);
    }

    public static IEnumerator Move(Transform rect, float time, float x, float y, bool is_unscaled = true)
    {
        float timer = 0.0f;
        Vector3 new_position;

        Vector3 initial = rect.position;
        Vector3 final = initial + new Vector3(x, y, 0);

        while (timer < time)
        {
            yield return null;
            if (rect == null)
            {
                break;
            }

            new_position = Vector3.Lerp(initial, final, timer / time);
            rect.position = new_position;
            timer += Time.deltaTime;
        }

        if (rect!= null)
            rect.position = final;
    }

    public static IEnumerator WaitForUnscaledSeconds(float time)
    {
        float timer = 0f;

        while (timer < time)
        {
            yield return null;
            timer += Time.unscaledDeltaTime;
        }
    }

    public static IEnumerator Lerp_Camera(Transform position0, Transform position1, float time)
    {
        Vector3 new_position = position0.position;
        Quaternion new_rotation = position0.rotation;

        float timer = 0f;

        while (timer < time)
        {
            Camera.main.transform.position = new_position;
            Camera.main.transform.rotation = new_rotation;

            yield return null;

            new_position = Vector3.Lerp(position0.position, position1.position, timer / time);
            new_rotation = Quaternion.Lerp(position0.rotation, position1.rotation, timer / time);

            timer += Time.unscaledDeltaTime;
        }

        Camera.main.transform.position = position1.position;
        Camera.main.transform.rotation = position1.rotation;
    }

    public static IEnumerator Autodestruct(GameObject other, float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(other);
    }
    
}



        /*public static GameObject Get_Closest(GameObject[] gameobject_list, Vector3 position)
    {
        GameObject closest_gameobject = gameobject_list[0];
        float distance = 10000;
        float new_distance;
        for (int i = 0; i < gameobject_list.Length; i++)
        {
            new_distance = (gameobject_list[i].transform.position - position).magnitude;
            if (new_distance < distance)
            {
                closest_gameobject = gameobject_list[i];
            }
        }

        return closest_gameobject;

}*/
