﻿using UnityEngine;
using System.Collections;

public class Player_Controller : MonoBehaviour {
    Rigidbody2D rb;
    float h;
    float speed = 12f;
    float jump_power = 16.2f;
    Vector2 jump_velocity;
    public int direction = 1;
    GameObject screech;
    int check_i = 0;
    bool[] notes = new bool[13];
    private SoundManager sounds;

    Object_Dictionary dictionary;
    UI_Manager ui_manager;
    Mechanic_Effects me;
    [HideInInspector]
    public bool can_teleport = false;
    public Transform teleport;

    [HideInInspector]
    public bool is_grounded = true;
    bool wobbling = false;
    Transform sprite;
    float buffer = 1f;
    //float v;

    // Use this for initialization
    void Start ()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        ui_manager = dictionary.Get_UI_Manager();
        sprite = transform.FindChild("Sprite");
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        me = GetComponent<Mechanic_Effects>();

        transform.position = dictionary.To_Checkpoint(check_i);

        screech = (GameObject)Resources.Load("Prefabs/Screech", typeof(GameObject));
        jump_velocity = new Vector2(0, jump_power);
        rb = GetComponent<Rigidbody2D>();
	}
	
    public void Add_Note(int note)
    {
        notes[note] = true;
        ui_manager.Add_Note(note);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (Utilities.Groundable(col.gameObject.tag))
        {
            //print("made true");
            //is_grounded = true;
            /*if (col.gameObject.tag == "Tile")
            {
                wobbling = false;
            }*/
        }
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (Utilities.Groundable(col.gameObject.tag))
        {
            //print("made true");
            //is_grounded = true;
            if (col.gameObject.tag == "Tile")
            {
                wobbling = false;
            }
        }
    }

    void OnCollisionExit2D(Collision2D col)
    {
        /*if (Utilities.Groundable(col.gameObject.tag))
        {
            is_grounded = false;
            print("degrounded");
        }*/
    }

    // Update is called once per frame
    void Update ()
    {
        Horizontal();

        if (Input.GetButtonDown("Screech"))
        {
            if (buffer > 1.5f && !wobbling)
            {
                StartCoroutine(sounds.play_Screech());
                StartCoroutine(Screech());
                buffer = 0f;

            }
            
        }

        if (Input.GetButtonDown("Bass"))
        {
            if (!wobbling)
            {
                StartCoroutine(Wobble());
            }
        }
        
        if (can_teleport)
        {
            print("ok");
            if (Input.GetButtonDown("Submit"))
            {
                print("submitted");
                // print("teleport");
                sounds.play_Portal();
                Teleport();
            }
        }
        buffer += 0.1f;

	}

    bool first;
    bool second;
    float offset = 0.8f;
    void LateUpdate()
    {
        hits = Physics2D.RaycastAll(transform.position + Vector3.right * offset, Vector3.down, 1.5f);

        foreach(RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {

                    if (hit.collider.tag == "Tile")
                    {
                        first = true;
                    }
                               
            }
        }

        hits = Physics2D.RaycastAll(transform.position - Vector3.right * offset, Vector3.down, 1.5f);

        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                if (hit.collider.tag == "Tile")
                {
                    second = true;
                }
            }
        }

        is_grounded = first || second;
        if (is_grounded)
        {
            wobbling = false;
        }
        first = false;
        second = false;

        if (Input.GetButtonDown("Jump"))
        {
            //print(is_grounded);
            if (is_grounded)
            {
                StartCoroutine(Jump());
            }
        }
        //print("made false");
        is_grounded = false;
    }

    void Teleport()
    {
        transform.position = teleport.position;
        rb.velocity = Vector3.zero;
    }

    RaycastHit2D[] hits;
    IEnumerator Jump()
    {
        //is_grounded = false;



        rb.velocity += jump_velocity;

        while (!is_grounded)
        {
            if (!wobbling)
            {
                rb.AddForce(Physics2D.gravity * 1);
            }
            yield return null;
        }

        yield return null;
    }

    IEnumerator Screech()
    {
        Quaternion rotation;

        if (direction == 1)
        {
            rotation = Quaternion.identity;
        }
        else
        {
            rotation = Quaternion.Euler(0, 0, 180);
        }

        Instantiate(screech, transform.position, rotation);
        yield return null;
    }

    IEnumerator Wobble()
    {
        me.Wobble(transform);
        StartCoroutine(sounds.Play_Wobble());
        wobbling = true;
        rb.velocity = Vector2.zero;
        //print("hello");
        float time = 0.3f;
        float timer = 0.0f;

        while ((timer < time))
        {
            //print("adding force");
            yield return null;
            rb.AddForce(-2f * Physics2D.gravity);
            timer += Time.deltaTime;
        }
    }

    Vector2 side_velocity;
    void Horizontal()
    {
        h = Input.GetAxis("Horizontal");
        side_velocity = new Vector2(h * speed, rb.velocity.y);
        rb.velocity = side_velocity;

        if (h * direction < 0)
        {
            Flip();
        }
    }

    void Flip()
    {
        sprite.localScale = new Vector3(-sprite.localScale.x, sprite.localScale.y, sprite.localScale.z);
        direction *= -1;
    }
}
