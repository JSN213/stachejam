﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Note_Controller : MonoBehaviour {
    int note;

    enum Note
    {
        C,
        Csharp,
        D,
        Dsharp,
        E,
        F,
        Fsharp,
        G,
        Gsharp,
        A,
        Asharp,
        B,
        Cup
    }

    Object_Dictionary dictionary;

    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        note = Random.Range(0, 12);
        Set_Color();
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(-5 * Time.deltaTime, 0, 0);
	}

    void Set_Color()
    {
        GetComponent<SpriteRenderer>().color = dictionary.Get_UI_Manager().Get_Note_Color(note);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Add_Note(other);
        }
        if (other.tag == "Player Collider")
        {
            Add_Note(other);
        }
    }

    void Add_Note(Collider2D other)
    {
        dictionary.Highlight_Zoom(gameObject);
        if (other.tag == "Player")
        {
            other.GetComponent<Player_Controller>().Add_Note(note);
        }
        if (other.tag == "Player Collider")
        {
            other.transform.parent.gameObject.GetComponent<Player_Controller>().Add_Note(note);
        }
        Destroy(gameObject);
    }
}
