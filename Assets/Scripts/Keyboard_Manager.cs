﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Keyboard_Manager : MonoBehaviour {
    Image[] notes;
    bool[] notes_on;
    bool[] hopping;
    int harmonic_i = -1;
    int[][] harmonics;
    SoundManager sounds;

    Vector3[] initials;

    Color[] colors;
    Object_Dictionary dictionary;

    GameObject key_pop;
    Mechanic_Effects me;

    public Image ltlr;

    void Start()
    {
        Utilities.Set_Transparency(ltlr, 0);
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        me = dictionary.Get_Player().GetComponent<Mechanic_Effects>();
        key_pop = (GameObject)Resources.Load("Mecaniques/KeyPop", typeof(GameObject));
        notes = new Image[transform.childCount];
        notes_on = new bool[transform.childCount];
        hopping = new bool[transform.childCount];
        colors = new Color[transform.childCount];
        initials = new Vector3[transform.childCount];
        int i = 0;
	    foreach (Transform child in transform)
        {
            notes[i] = child.gameObject.GetComponent<Image>();
            colors[i] = notes[i].color;
            notes[i].color = Color.white;
            initials[i] = child.position;
            i++;
        }

        Set_Harmonics();
	}

    bool isfirst = false;
	// Update is called once per frame
	void Update () {


        if (true)
        {
            if (harmonic_i != -1)
            {
                if (Jam_Input())
                {
                    Jam();
                }
            }
        }
        else
        {
            harmonic_i = 1;
            if (Jam_Input())
            {
                Jam();
            }
        }
	}

    Color new_color;
    public void Display_Note(int note)
    {
        /*new_color = notes[note].color;
        new_color *= 2;*/
        sounds.play_GetNote(note);
        notes[note].color = colors[note];
        notes_on[note] = true;
        dictionary.Highlight_Zoom(notes[note]);
        Check_Harmonics();

        for (int i = 1; i < notes_on.Length; i++)
        {
            //print(notes_on[i]);
        }
    }

    public void Hide_Note(int note)
    {
        /*new_color = notes[note].color;
        new_color /= 2;*/
        notes[note].color = Color.white;
        notes_on[note] = false;
    }

    void Check_Harmonics()
    {
        bool any_available = false;
        bool available = true;

        for (int i = 0; i < harmonics.Length; i++)
        {
            //print(i);
            available = true;
            for (int j = 0; j < harmonics[i].Length; j++)
            {
                if (!notes_on[harmonics[i][j]])
                    available = false;
            }

            if (available)
            {
                print(i);
                print(harmonics[i]);
                print("Avaialabal");

                if (harmonic_i != -1)
                {
                    for (int j = 0; j < harmonics[harmonic_i].Length; j++)
                    {
                        hopping[harmonics[harmonic_i][j]] = false;
                    }
                }

                harmonic_i = i;
                
                Utilities.Set_Transparency(ltlr, 1);
                Display_Harmonic();
                any_available = true;
                break;
            }
        }

        if (!any_available)
        {
            harmonic_i = -1;
            Utilities.Set_Transparency(ltlr, 0);
        }
    }

    void Jam()
    {
        
        sounds.play_NoteBurst(harmonics[harmonic_i]);
        StartCoroutine(Display_Jam());
        dictionary.Get_Health_Manager().Max_Health();
        Disable_Harmonic();
        Check_Harmonics();
    }

    float pop_delay = .1f;

    IEnumerator Display_Jam()
    {
        dictionary.Destroy_Visible();
        int index = harmonic_i;

        for (int i = 0; i < 3; i++)
        {
            print("jammin" + i);
            int note = harmonics[index][i];
            notes[note].gameObject.GetComponent<Mechanic_Effects>().Wobble(notes[note].transform);
            yield return new WaitForSeconds(pop_delay);
        }

    }

    void Disable_Harmonic()
    {
        for (int i = 0; i < harmonics[harmonic_i].Length; i++)
        {
            Hide_Note(harmonics[harmonic_i][i]);
            hopping[harmonics[harmonic_i][i]] = false;
        }
    }

    void Set_Harmonics()
    {
        harmonics = new int[24][];

        harmonics[0] = new int[]   { 0, 4, 7 };
        harmonics[1] = new int[]   { 1, 5, 8 };
        harmonics[2] = new int[]   { 2, 6, 9 };
        harmonics[3] = new int[]   { 3, 7, 10};
        harmonics[4] = new int[]   { 4, 8, 11};
        harmonics[5] = new int[]   { 5, 9, 0};
        harmonics[6] = new int[]   { 6, 10, 1};
        harmonics[7] = new int[]   { 7, 11, 2 };
        harmonics[8] = new int[]   { 8, 0, 3 };
        harmonics[9] = new int[]   { 9, 1, 4 };
        harmonics[10] = new int[]  { 10, 2, 5};
        harmonics[11] = new int[]  { 11, 3, 6 };

        harmonics[12] = new int[] { 11, 2, 6 };
        harmonics[13] = new int[] { 0, 3, 7 };
        harmonics[14] = new int[] { 1, 4, 8 };
        harmonics[15] = new int[] { 2, 5, 9 };
        harmonics[16] = new int[] { 3, 6, 10 };
        harmonics[17] = new int[] { 4, 7, 11 };
        harmonics[18] = new int[] { 5, 8, 0 };
        harmonics[19] = new int[] { 6, 9, 1 };
        harmonics[20] = new int[] { 7, 10, 2 };
        harmonics[21] = new int[] { 8, 11, 3 };
        harmonics[22] = new int[] { 9, 0, 4 };
        harmonics[23] = new int[] { 10, 1, 5 };
    }

    void Display_Harmonic()
    {
        print("Harmonic available effect");

        int note;
        for (int i = 0; i < harmonics[harmonic_i].Length; i++)
        {
            note = harmonics[harmonic_i][i];
            if (!hopping[note])
            {
                hopping[note] = true;
                StartCoroutine(Hop(note));
            }
        }
    }

    float hop_time = 1f;
    float height = 20;
    IEnumerator Hop(int note)
    {
        Vector3 initial = initials[note];
        Vector3 final = notes[note].transform.position + Vector3.up * height;
        float timer = 0f;
        float t;

        bool finish = false;

        while (hopping[note])
        {
            timer = 0f;
            while (timer < hop_time)
            {
                yield return null;
                t = -4 * (timer - hop_time / 2f) * (timer - hop_time / 2f) / (hop_time * hop_time) + 1;
                notes[note].transform.position = Vector3.Lerp(initial + Vector3.back, final, t);

                timer += Time.deltaTime;
            }
            finish = true;
        }

        if (finish)
        {
            while (timer < hop_time)
            {
                yield return null;
                t = -4 * (timer - hop_time / 2f) * (timer - hop_time / 2f) / (hop_time * hop_time) + 1;
                notes[note].transform.position = Vector3.Lerp(initial + Vector3.back, final, t);

                timer += Time.deltaTime;
            }
        }
        notes[note].transform.position = initial;
    }

    bool jammingdown;
    bool Jam_Input()
    {
        bool input = false;

        if ((0 != Input.GetAxis("Jam1")) && (0 != Input.GetAxis("Jam2")))
        {
            if (!jammingdown)
            {
                input = true;
                jammingdown = true;
            }
        }
        else
        {
            jammingdown = false;
        }

        return input;
    }

    public Color Get_Note_Color(int i)
    {
        return colors[i];
    }
}
