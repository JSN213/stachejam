﻿using UnityEngine;
using System.Collections;

public class Screecher : MonoBehaviour {
    float speed = 20;
    int direction = 1;
    SoundManager sounds;

    // Use this for initialization
    void Start () {
        direction = GameObject.Find("Player").GetComponent<Player_Controller>().direction;
        StartCoroutine(Utilities.Autodestruct(gameObject, .3f));
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        int n = Random.Range(0, 4);
        Color[] colors = new Color[] { Color.red, Color.yellow, Color.green, Color.cyan };

        GetComponentInChildren<SpriteRenderer>().color = colors[n] + Color.gray/2;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += new Vector3(speed * direction * Time.deltaTime, 0, 0);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Wall")
        {
            sounds.Play_VitreBriser();
            Destroy(other.gameObject);
        }
        else if (other.tag == "Turret")
        {
            sounds.Destroy_Turret();
            Destroy(other.transform.parent.gameObject);
        }
        else if (other.tag == "Saucer")
        {
            sounds.Destroy_Spaceship();
            Destroy(other.transform.parent.gameObject);
        }
    }
}
