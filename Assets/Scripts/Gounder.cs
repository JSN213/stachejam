﻿using UnityEngine;
using System.Collections;

public class Gounder : MonoBehaviour {
    Player_Controller player_controller;

    // Use this for initialization
    void Start () {
        player_controller = GameObject.Find("Player").GetComponent<Player_Controller>();
	}

    void Update()
    {
        //player_controller.is_grounded = false;
    }
	
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D other) {
        print("Gello");
        if (Utilities.Groundable(other.tag))
        {
            print("Using trig");
            player_controller.is_grounded = true;
        }
	}
}
