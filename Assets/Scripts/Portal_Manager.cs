﻿using UnityEngine;
using System.Collections;

public class Portal_Manager : MonoBehaviour {
    GameObject[] pairs;
    


	// Use this for initialization
	void Start () {
        pairs = new GameObject[transform.childCount];

        int i = 0;
        foreach (Transform child in transform)
        {
            pairs[i] = child.gameObject;
            Set_Pair(pairs[i]);
            i++;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    Portal_Controller recto;
    Portal_Controller verso;
    void Set_Pair(GameObject pair)
    {
        recto = pair.transform.Find("Recto").gameObject.GetComponent<Portal_Controller>();
        verso = pair.transform.Find("Verso").gameObject.GetComponent<Portal_Controller>();
        
        recto.Setup(verso.transform);
        verso.Setup(recto.transform);
    }
}
