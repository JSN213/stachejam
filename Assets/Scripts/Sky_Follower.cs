﻿using UnityEngine;
using System.Collections;

public class Sky_Follower : MonoBehaviour {
    float speed = 0.9f;

    Object_Dictionary dictionary;
    Transform player;

    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        player = dictionary.Get_Player().transform;
    }

    // Update is called once per frame
    Vector3 new_position;
	void LateUpdate () {
        new_position = Camera.main.transform.position * speed;
        new_position.z = transform.position.z;
        transform.position = new_position;
	}
}
