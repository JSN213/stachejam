﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Banner : MonoBehaviour {
    bool is_typing;
    Text text;
    float buffer = .1f;
    float timer = 0.0f;

    string[] cheers = 
    {
        "Groovy",
        "Radical"
    };

	// Use this for initialization
	void Start () {
        text = gameObject.GetComponent<Text>();
        text.text = "1234578          ";
        Trigger_Cheer();
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

	    if (!is_typing)
        {
            if (timer > buffer)
            {
                Add_Queue(' ');
                timer -= buffer;
            }
        }
	}

    void Trigger_Cheer()
    {
        StartCoroutine(Cheer());
    }

    IEnumerator Cheer()
    {
        string cheer = cheers[Random.Range(0, cheers.Length)];

        while (is_typing)
        {
            yield return null;
        }

        StartCoroutine(Type(cheer));
    }

    IEnumerator Type(string _text)
    {
        is_typing = true;

        foreach (char letter in _text)
        {
            while (timer < buffer)
            {
                yield return null;
            }

            timer -= buffer;

            Add_Queue(letter);
            yield return null;
        }

        while (timer < buffer)
        {
            yield return null;
        }

        timer -= buffer;
        Add_Queue(' ');
        yield return null;
        is_typing = false;
    }

    string new_string;

    public void Add_Queue(char letter)
    {
        //print(text.text);
        new_string = text.text.Substring(1, text.text.Length-1);
        new_string += letter;
        text.text = new_string;
    }
}
