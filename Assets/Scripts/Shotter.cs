﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shotter : MonoBehaviour {
    float speed = 10;
    float angle_speed = 100;
    public int directionx = 0;
    public int directiony = 0;

    float time_destroy = 1.5f;

    public Sprite[] sprites = new Sprite[3];

    Object_Dictionary dictionary;
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        StartCoroutine(Utilities.Autodestruct(gameObject, time_destroy));

        int n = Random.Range(0, 3);

        GetComponent<SpriteRenderer>().sprite = sprites[n];
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(speed * directionx * Time.deltaTime, speed * directiony * Time.deltaTime, 0);
        transform.rotation *= Quaternion.Euler(new Vector3(0, 0, angle_speed * (directionx - directiony) * Time.deltaTime));
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Tile")
        {
            Destroy(gameObject);
        }
        else if (other.tag == "Player")
        {
            dictionary.Get_Health_Manager().Take_Damage(1);

            Destroy(gameObject);
        }
    }
}
