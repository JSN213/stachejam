﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Portal_Controller : MonoBehaviour {
    Transform target;
    SoundManager sounds;

    public Image[] images;

	// Use this for initialization
	void Start () {
        player_controller = GameObject.Find("Player").GetComponent<Player_Controller>();
        
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Setup(Transform _target)
    {
        target = _target;
    }

    Player_Controller player_controller;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            print(!player_controller.can_teleport);
            if (!player_controller.can_teleport)
            {
                player_controller.can_teleport = true;
                
                player_controller.teleport = target;
            }
            else
            {
                print("trigger");
                player_controller.can_teleport = false;
            }


            if (transform.parent.name == "Fin")
            {
                StartCoroutine(Fin());
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player_controller.can_teleport = false;
        }
    }

    IEnumerator Fin()
    {
        Time.timeScale = 0;
        /*for (int i = 0; i < images.Length; i++)
        {
            Utilities.Set_Transparency(images[i], 1);
            //StartCoroutine(Utilities.Fade(images[i], time_fade, 1, 0, true));
        }*/
        //yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(2));// WaitForSeconds(2);
        float time_fade = 1f;

        for (int i = 0; i < images.Length; i++)
        {
            //Utilities.Set_Transparency(images[i], 1);
            StartCoroutine(Utilities.Fade(images[i], time_fade, 0, 1, true));
        }
        yield return StartCoroutine(Utilities.WaitForUnscaledSeconds(time_fade));// WaitForSeconds(2);
        //Time.timeScale = 1;
    }
}
