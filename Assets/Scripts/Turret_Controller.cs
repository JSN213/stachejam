﻿using UnityEngine;
using System.Collections;

public class Turret_Controller : MonoBehaviour {
    Transform shot_geo;
    SoundManager sounds;
    private AudioSource shoot_source;
    public float shoot_seconds;
    int n_shots = 3;
    float buffer = 0.5f;
    GameObject shot;
    public int facingx = 0;
    public int facingy = 0;
    // Use this for initialization
    public bool visible;

    void Start () {
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        shot_geo = transform.FindChild("Shot Geometry");
        shot = (GameObject)Resources.Load("Prefabs/Shot", typeof(GameObject));
        shoot_source = GetComponent<AudioSource>();
        if (shoot_source.ToString() == "null")
        {
            shoot_source = gameObject.AddComponent<AudioSource>();
            shoot_source.outputAudioMixerGroup = sounds.Get_ShootMixer();
            shoot_source.spatialBlend = 1f;
            shoot_source.rolloffMode = AudioRolloffMode.Linear;
            shoot_source.mute = false;

        }
        shoot_source.mute = true;

        StartCoroutine(Shoot());
	}
	
	// Update is called once per frame
	void Update () {
	    if (visible)
        {

            shoot_source.mute = false;
        }
        else
        {

            shoot_source.mute = true;
        }
	}
    

    Transform clone;
    IEnumerator Shoot()
    {
        while (true)
        {
            for (int i = 0; i < n_shots; i++)
            {
                clone = ((GameObject)Instantiate(shot, shot_geo.position, transform.rotation)).transform;
                clone.gameObject.GetComponent<Shotter>().directionx = facingx;
                clone.gameObject.GetComponent<Shotter>().directiony = facingy;
                sounds.play_shoot(shoot_source);
                //clone.parent = transform;
                yield return new WaitForSeconds(buffer);
            }
            Flip();
        }
    }

    void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, 1, 1);
        facingx *= -1;
        facingy *= -1;
    }
    
}
