﻿using UnityEngine;
using System.Collections;

public class Tile_Manager : MonoBehaviour {
    bool is_row;
    bool is_surrounded;

    bool[] neighbours = new bool[4] { false, false, false, false };
    Vector2[] directions = new Vector2[4];
    BoxCollider2D box;

    SpriteRenderer sprite;
    public Sprite tile_row;
    public Sprite tile_block;

	// Use this for initialization
	void Start () {
        sprite = transform.FindChild("Sprite").GetComponent<SpriteRenderer>();
        sprite.sprite = tile_block;

        if (GetComponent<BoxCollider2D>() == null)
        {
            box = gameObject.AddComponent<BoxCollider2D>();
        } else
        {
            box = GetComponent<BoxCollider2D>();
        }
        if (GetComponent<PlatformEffector2D>() == null)
        {
            gameObject.AddComponent<PlatformEffector2D>().useOneWay = false;
            box.usedByEffector = true;
        }

        directions[0] = new Vector2(0, 1);
        directions[1] = new Vector2(0, -1);
        directions[2] = new Vector2(1, 0);
        directions[3] = new Vector2(-1, 0);

        Align();
        Set_Neighbours();

        Set_Properties();
        Set_Colliders();


	}
	
	// Update is called once per frame
	void Update () {
	
	}

    Vector3 new_position;
    void Align()
    {
        new_position = transform.position;
        new_position.x = Mathf.Round(new_position.x);
        new_position.y = Mathf.Round(new_position.y);
        transform.position = new_position;
    }

    RaycastHit2D hit;
    void Set_Neighbours()
    {
        for (int i = 0; i < 4; i++)
        {
            hit = Physics2D.Raycast((Vector2)transform.position + directions[i] * 0.6f, directions[i] * 0.4f, 1);

            if (hit.collider != null)
            {
                if (hit.collider.tag == "Tile")
                {
                    neighbours[i] = true;
                }
            }
        }
    }

    void Set_Properties()
    {
        is_surrounded = true;
        
        foreach (bool neighbour in neighbours)
        {
            if (!neighbour) is_surrounded = false;
        }
        if (!neighbours[0] && !neighbours[1])
        {
            is_row = true;
            /*gameObject.AddComponent<PlatformEffector2D>().surfaceArc = 160;
            box.usedByEffector = true;*/
        }
    }

    void Set_Colliders()
    {
        if (is_surrounded)
        {
            Destroy(GetComponent<PlatformEffector2D>());
            Destroy(GetComponent<BoxCollider2D>());
        }

        if (is_row)
        {
            box.usedByEffector = true;
            gameObject.GetComponent<PlatformEffector2D>().surfaceArc = 160;
            gameObject.GetComponent<PlatformEffector2D>().useOneWay = true;
            sprite.sprite = tile_row;
        }
    }
}
