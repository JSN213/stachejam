﻿using UnityEngine;
using System.Collections;

public class Game_Controller : MonoBehaviour {
    Object_Dictionary dictionary;
    UI_Manager ui_manager;

	// Use this for initialization
	void Start () {

        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();  
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Die()
    {
        dictionary.Get_Player().transform.position = dictionary.To_Checkpoint(0);
        dictionary.Get_Health_Manager().Max_Health();
    }
}
