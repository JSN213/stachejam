﻿using UnityEngine;
using System.Collections;

public class Camera_Controller : MonoBehaviour {
    float dampTime = .3f;
    private Vector3 velocity = Vector3.zero;
    Transform target;

    float min_y = -5;
    float max_y = 20000;

    Camera cam;

    void Start()
    {
        cam = Camera.main;
        target = GameObject.Find("Camera Target").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //print("ici");
        if (target)
        {
           // print("eh oui");
            Vector3 point = cam.WorldToViewportPoint(target.position);
            Vector3 delta = target.position - cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
        }

        Clamp();
    }

    void Clamp()
    {
        if (transform.position.y < min_y)
        {
            Vector3 new_position = transform.position;
            new_position.y = min_y;
            transform.position = new_position;
        }
        if (transform.position.y > max_y)
        {
            Vector3 new_position = transform.position;
            new_position.y = max_y;
            transform.position = new_position;
        }
    }
}
