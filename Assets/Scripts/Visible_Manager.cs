﻿using UnityEngine;
using System.Collections;

public class Visible_Manager : MonoBehaviour {
    Object_Dictionary dictionary;

    // Use this for initialization
    void Start()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
    }
	
	// Update is called once per frame
	void OnBecameVisible()
    {
        if (tag == "Turret")
        {
            print("vbisible");
            transform.parent.gameObject.GetComponent<Turret_Controller>().visible = true;
        }
        else if (tag == "Saucer")
        {

            print("vbisible");
            transform.parent.gameObject.GetComponent<Saucer_Controller>().visible = true;
        }
        dictionary.Set_Visible(gameObject);
    }

    void OnBecameInvisible()
    {
        if (tag == "Turret")
        {
            print("invbisible");
            transform.parent.gameObject.GetComponent<Turret_Controller>().visible = false;
        }
        dictionary.Set_Invisible(gameObject);
    }
}
