﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Health_Manager : MonoBehaviour {
    int max_health = 3;
    UI_Manager ui_manager;
    Object_Dictionary dictionary;
    SoundManager sounds;
    int health;
    SpriteRenderer sprite;

	// Use this for initialization
	void Start ()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        ui_manager = dictionary.Get_UI_Manager();

        last_glow = GameObject.Find("Last Glow").GetComponent<Image>();

        health = max_health;
        ui_manager.Update_Health(health);
    }

    Image last_glow;
    float beep_buffer = .5f;
    // Update is called once per frame
    int last_health;
	void Update () {
	    if (health == 1)
        {
            if (last_health != 1)
            {
                sounds.Start_OneHeart();
            }

            if (Mathf.RoundToInt(Time.time/beep_buffer) % 2 == 0)
            {
                Utilities.Set_Transparency(last_glow, 0);
            } else
            {
                if (last_glow.color.a == 0)
                {

                }
                Utilities.Set_Transparency(last_glow, 1);
            }
        }
        else
        {
            if (last_health == 1)
            {
                sounds.Stop_OneHeart();
            }
        }
        last_health = health;
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Shot")
        {
            Take_Damage(1);
            Destroy(col.gameObject);
        }
    }

    public void Take_Damage(int i)
    {
        health -= i;
        health = Mathf.Clamp(health, 0, max_health);
        sounds.play_PlayerHurt();
        ui_manager.Update_Health(health);

        StartCoroutine(FadeRed());

        if (health == 0)
        {
            sounds.play_PlayerHurt();
            Die();
        }
    }

    IEnumerator FadeRed()
    {
        float timess = 0.2f;

        float timer = 0f;

        sprite.color = Color.red;
        while (timer < timess)
        {
            yield return null;
            sprite.color = Color.Lerp(Color.red, Color.white, timer / timess);
            timer += Time.deltaTime;
        }

        sprite.color = Color.white;
    }

    public void Max_Health()
    {
        health = max_health;
        ui_manager.Update_Health(health);
    }

    void Die()
    {
        
        dictionary.Get_Game_Controller().Die();
    }
}
