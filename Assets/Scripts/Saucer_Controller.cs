﻿using UnityEngine;
using System.Collections;

public class Saucer_Controller : MonoBehaviour {
    float move_time = 0.3f;
    int n_shots = 3;
    float buffer = 0.5f;
    int pos_i = 0;
    float speed = 5;
    public bool shoot_left =false;

    SoundManager sounds;
    private AudioSource shoot_source;
    public Transform[] waypoints;
    float angle;

    Transform shot_geo;
    GameObject shot;

    void Start ()
    {
        sounds = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        transform.position = waypoints[0].position;

        shot_geo = transform.FindChild("Shot Geometry");
        shot = (GameObject)Resources.Load("Prefabs/Shot", typeof(GameObject));
        shoot_source = GetComponent<AudioSource>();
        if (shoot_source.ToString() == "null")
        {
            shoot_source = gameObject.AddComponent<AudioSource>();
            shoot_source.outputAudioMixerGroup = sounds.Get_ShootMixer();
            shoot_source.spatialBlend = 1f;
            shoot_source.rolloffMode = AudioRolloffMode.Linear;
            shoot_source.mute = false;

        }
        shoot_source.mute = true;

        StartCoroutine(Patrol());
	}

    public bool visible;
	// Update is called once per frame
	void Update ()
    {
        if (visible)
        {

            shoot_source.mute = false;
        }
        else
        {

            shoot_source.mute = true;
        }
    }

    Transform clone;
    IEnumerator Patrol()
    {
        while (true)
        {
            for (int i = 0; i < n_shots; i++)
            {
                clone = ((GameObject)Instantiate(shot, shot_geo.position, shot_geo.rotation)).transform;
                if (!shoot_left)
                {
                    clone.GetComponent<Shotter>().directionx = 1;
                    sounds.play_shoot(shoot_source);
                }
                if (shoot_left)
                {
                    clone.GetComponent<Shotter>().directionx = -1;
                    sounds.play_shoot(shoot_source);
                }

                    
                
                yield return new WaitForSeconds(buffer);
            }

            yield return StartCoroutine(Progress());
        }
    }

    Vector3 next_position;
    IEnumerator Progress()
    {
        pos_i++;
        pos_i %= waypoints.Length;
        next_position = waypoints[pos_i].position;

        while (transform.position != next_position)
        {
            yield return null;
            transform.position = Vector3.MoveTowards(transform.position, next_position, speed * Time.deltaTime);
        }
    }
}
