﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour {
    //Image[] notes;
    Image[] hearts;
    Image[] glows;
    Keyboard_Manager kb_manager;

	// Use this for initialization
	void Awake () {
        kb_manager = GameObject.Find("Keyboard").gameObject.GetComponent<Keyboard_Manager>();

        Transform temp = GameObject.Find("Health").transform;
        hearts = new Image[temp.childCount];
        glows = new Image[temp.childCount];

        int i = 0;
        foreach (Transform child in temp)
        {
            hearts[i] = child.gameObject.GetComponent<Image>();
            glows[i] = child.GetChild(0).GetComponent<Image>();
            i++;
        }
    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void Add_Note(int note)
    {
        kb_manager.Display_Note(note);
    }

    public void Update_Health(int health)
    {
        foreach (Image heart in hearts)
        {
            heart.color = Color.black;
        }
        foreach (Image glow in glows)
        {
            Utilities.Set_Transparency(glow, 0);
        }

        for (int i = 0; i < health; i++)
        {
            hearts[i].color = Color.red;
            Utilities.Set_Transparency(glows[i], 1);
        }
    }

    public Color Get_Note_Color(int i)
    {
        return kb_manager.Get_Note_Color(i);
    }
}
