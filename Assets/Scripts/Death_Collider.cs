﻿using UnityEngine;
using System.Collections;

public class Death_Collider : MonoBehaviour {
    Object_Dictionary dictionary;

	// Use this for initialization
	void Start ()
    {
        dictionary = GameObject.Find("Game Controller").GetComponent<Object_Dictionary>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            dictionary.Get_Game_Controller().Die();
        }
    }
}
